﻿INSERT
	INTO dbo.Permission
	(CanRequest, RequestDuration, NeedsAuth, IsAuthorizer)
	VALUES
	(1, 72, 0, 1),
	(1, 72, 0, 0);
GO
INSERT
	INTO dbo.Groups
	(SID, Active, PermissionID)
	VALUES
	('S-1-5-21-1960408961-1123561945-725345543-45152', 1, 1),
	('S-1-5-21-1960408961-1123561945-725345543-45151', 1, 2);
GO
--INSERT
--	INTO dbo.Users
--	(Username, GroupID)
--	VALUES
--	('NO_AUTH', 1),
--	('ZA\spurnell', 1);
--GO

--INSERT
--	INTO dbo.Users
--	(Username, GroupID)
--	VALUES
--	('NO_AUTH', 1);

--INSERT
--	INTO dbo.Request
--	(Username, UserEMail, Duration, Status, RequestName, RequestCompany, Authorizer, TimeRequested, EMailedRequesterFlag, EMailedAuthFlag)
--	VALUES
--	('ZA\smith', 'spurnell@illovo.co.za', 24, 2, 'Jane Doe', 'Cyberdyne Defence Systems', NULL, GETDATE(), 0, 0);
--SELECT *
--	FROM dbo.Request;

INSERT
	INTO dbo.Config
	(WebServerURL, AddGroupSID, Domain, AccountOU, SmtpServerHost, SmtpServerPort, SmtpFromAddress)
	VALUES
	('http://hoapp1/WiFiUserAuthHO/', 'S-1-5-21-1960408961-1123561945-725345543-45153',
	'za.illovo.net', 'OU=WiFi Accounts,OU=Head Office,DC=za,DC=illovo,DC=net', 'exchhub.za.illovo.net', 25, 'noreply-wireless@illovo.co.za');