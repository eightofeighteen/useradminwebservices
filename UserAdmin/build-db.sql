﻿DROP TABLE dbo.Request;
GO
--DROP TABLE dbo.Users;
--GO
DROP TABLE dbo.Groups;
GO
DROP TABLE dbo.Permission;
GO
CREATE TABLE
	dbo.Permission (
		ID INT IDENTITY(1, 1) NOT NULL,
		CanRequest BIT NOT NULL,
		RequestDuration INT,
		NeedsAuth BIT NOT NULL,
		IsAuthorizer BIT NOT NULL,
		PRIMARY KEY CLUSTERED (ID ASC) );

CREATE TABLE
	dbo.Groups (
		ID INT IDENTITY(1, 1) NOT NULL,
		SID varchar(128) NOT NULL,
		Active BIT NOT NULL,
		PermissionID INT NOT NULL,
		PRIMARY KEY CLUSTERED (ID ASC),
		CONSTRAINT [FK_dbo.Groups_dbo.Permission_ID] FOREIGN KEY (PermissionID)
		REFERENCES dbo.Permission (ID) );

--CREATE TABLE
--	dbo.Users (
--		ID INT IDENTITY(1, 1) NOT NULL,
--		UserName varchar(128) NOT NULL,
--		GroupID INT NOT NULL,
--		PRIMARY KEY CLUSTERED (ID ASC),
--		CONSTRAINT [FK_dbo.Users_dbo.Groups_ID] FOREIGN KEY (GroupID)
--		REFERENCES dbo.Groups (ID) );

CREATE TABLE
	dbo.Request (
		ID INT IDENTITY(1, 1) NOT NULL,
		Username varchar(128),
		UserEMail varchar(128) NOT NULL,
		Duration INT NOT NULL,
		Status INT NOT NULL,
		GenUserName varchar(128),
		GenPassword varchar(128),
		RequestName varchar(128) NOT NULL,
		RequestCompany varchar(128) NOT NULL,
		Authorizer varchar(128),
		TimeRequested datetime2,
		TimeApproved datetime2,
		TimeCompleted datetime2,
		EMailedAuthFlag BIT NOT NULL,
		EMailedRequesterFlag BIT NOT NULL,
		PRIMARY KEY CLUSTERED (ID ASC));

CREATE TABLE
       dbo.Config (
       ID INT IDENTITY(1, 1) NOT NULL,
       WebServerURL varchar(128) NOT NULL,
       AddGroupSID varchar(128) NOT NULL,
       Domain varchar(128) NOT NULL,
       AccountOU varchar(256) NOT NULL,
       SmtpServerHost varchar(128) NOT NULL,
       SmtpServerPort int NOT NULL,
       SmtpFromAddress varchar(128) NOT NULL,
       PRIMARY KEY CLUSTERED (ID ASC));
