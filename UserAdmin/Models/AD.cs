﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.DirectoryServices;
using System.DirectoryServices.ActiveDirectory;
using System.DirectoryServices.AccountManagement;

namespace UserAdmin.Models
{
    static public class AD
    {
        static public string GetEmailAddress(string userName)
        {
            string email = "";
            List<GroupPrincipal> result = new List<GroupPrincipal>();

            // establish domain context

            PrincipalContext yourDomain = new PrincipalContext(ContextType.Domain);

            // find your user
            UserPrincipal user = UserPrincipal.FindByIdentity(yourDomain, userName);

            if (user != null)
            {
                email = user.EmailAddress;
            }

            return email;
        }

        static public List<GroupPrincipal> GetGroups(string userName)
        {
            List<GroupPrincipal> result = new List<GroupPrincipal>();

            // establish domain context
            
            PrincipalContext yourDomain = new PrincipalContext(ContextType.Domain);

            // find your user
            UserPrincipal user = UserPrincipal.FindByIdentity(yourDomain, userName);

            // if found - grab its groups
            if (user != null)
            {
                PrincipalSearchResult<Principal> groups = user.GetAuthorizationGroups();

                // iterate over all groups
                foreach (Principal p in groups)
                {
                    // make sure to add only group principals
                    if (p is GroupPrincipal)
                    {
                        result.Add((GroupPrincipal)p);
                    }
                }
            }

            return result;
        }

        static public List<String> Groups(String userName, Boolean recursive)
        {
            List<String> groups = new List<String>();
            List<GroupPrincipal> groupPrincipals = GetGroups(userName);
            foreach (GroupPrincipal principal in groupPrincipals)
                groups.Add(principal.Sid.ToString());
            return groups;
        }
    }
}