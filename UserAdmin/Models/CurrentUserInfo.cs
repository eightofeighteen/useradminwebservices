﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UserAdmin.Models
{
    public class CurrentUserInfo
    {
        public int ID { get; set; }
        public String userName { get; set; }
        public String groupSID { get; set; }
        public Boolean isActive { get; set; }
        public Boolean canRequest { get; set; }
        public int maxRequest { get; set; }
        public Boolean needsAuth { get; set; }
        public Boolean isAuth { get; set; }
        public String SID { get; set; }
    }
}