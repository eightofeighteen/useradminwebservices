﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using UserAdmin.Models;

namespace UserAdmin.Controllers
{
    public class RequestsController : Controller
    {
        private UserAdminDevEntities db = new UserAdminDevEntities();

        private enum REQUEST_STATUS { NO_STATUS, PENDING, AUTH_REQ, COMPLETE, AUTH_DECLINED, AUTH_APPROVED, CLEANED };

        

        public ActionResult ListGroups()
        {
            return View(db.Groups.ToList());
        }

        private CurrentUserInfo getCurrentUserInfo()
        {
            CurrentUserInfo info = new CurrentUserInfo();
            List<String> groupSIDs = AD.Groups(User.Identity.Name, false);
            //db.Database.Connection.Open();
            var gquery =
                from a in db.Groups
                where (groupSIDs.Contains(a.SID))
                select a;
            if (gquery.Count() > 0)
            {
                foreach (var gEntry in gquery)
                {
                    info.groupSID = gEntry.SID;
                    info.canRequest = gEntry.Permission.CanRequest;
                    info.isActive = gEntry.Active;
                    info.isAuth = gEntry.Permission.IsAuthorizer;
                    info.needsAuth = gEntry.Permission.NeedsAuth;
                    info.maxRequest = (int)gEntry.Permission.RequestDuration;
                    break;
                }
            }
            else
            {
                // Error.
                return null;
            }
            /*var uquery =
                from a in db.Users
                where (a.UserName == User.Identity.Name)
                select a;

            if (uquery.Count() > 0)
            {
                foreach (var entry in uquery)
                {
                    info.userName = entry.UserName;
                    var gquery =
                        from a in db.Groups
                        where (a.ID == entry.GroupID)
                        select a;
                    if (gquery.Count() > 0)
                    {
                        foreach (var gEntry in gquery)
                        {
                            info.groupSID = gEntry.SID;
                            info.canRequest = gEntry.Permission.CanRequest;
                            info.isActive = gEntry.Active;
                            info.isAuth = gEntry.Permission.IsAuthorizer;
                            info.needsAuth = gEntry.Permission.NeedsAuth;
                            info.maxRequest = (int)gEntry.Permission.RequestDuration;
                            break;
                        }
                    }
                    else
                    {
                        // Error.
                        return null;
                    }
                    break;
                }
            }
            else
            {
                // Error.
                return null;
            }*/

            return info;
        }

        public ActionResult UserInfo()
        {
            CurrentUserInfo info = getCurrentUserInfo();
            if (info == null)
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
            return View(info);
        }

        public ActionResult New()
        {
            CurrentUserInfo info = getCurrentUserInfo();
            if (info == null)
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
            return View(info);
        }

        //private int getUserIDforUserName(String username)
        //{
        //    var query =
        //            from a in db.Users
        //            where (a.UserName == username)
        //            select a;
        //    if (query.Count() > 0)
        //    {
        //        return query.ToList()[0].ID;
        //    }
        //    return 0;
        //}

        //private int getUserID(String username = "")
        //{
        //    if (String.IsNullOrEmpty(username))
        //        return getUserIDforUserName(User.Identity.Name);
        //    return getUserIDforUserName(username);
        //}

        private string getEMailforUserName(String username)
        {
            /*if (username.Contains('\\'))
                return username.Split('\\')[1] + "@illovo.co.za";
            return "no-addr@acme-corp.local";*/
            string email = AD.GetEmailAddress(username);
            if (!String.IsNullOrEmpty(email))
                return email;
            return "no-addr@acme-corp.local";
        }

        private string getEMailforUser(String username = "")
        {
            if (String.IsNullOrEmpty(username))
                return getEMailforUserName(User.Identity.Name);
            return getEMailforUserName(username);
        }

        //private int BlankAuthID()
        //{
        //    var query =
        //        from a in db.Users
        //        where (a.UserName == "NO_AUTH")
        //        select a;
        //    if (query.Count() > 0)
        //        return query.ToList()[0].ID;
        //    return 0;
        //}

        private Boolean ValidRequest(String username, String company, int duration)
        {
            var query =
                from a in db.Requests
                where (a.RequestName.ToLower() == username.ToLower() && a.RequestCompany.ToLower() == company.ToLower() && a.Status != (int)REQUEST_STATUS.AUTH_DECLINED)
                select a;
            foreach (var elem in query)
                if (elem.TimeRequested.Value.AddHours(elem.Duration) > DateTime.Now)
                    return false;
            return true;
        }

        private int DuplicateRequestID(String username, String company, int duration)
        {
            var query =
                from a in db.Requests
                where (a.RequestName.ToLower() == username.ToLower() && a.RequestCompany.ToLower() == company.ToLower())
                select a;
            foreach (var elem in query)
                if (elem.TimeRequested.Value.AddHours(elem.Duration) > DateTime.Now)
                    return elem.ID;
            return 0;
        }

        public ActionResult DuplicateRequest(int id = 0)
        {
            var query =
                from a in db.Requests
                where (a.ID == id)
                select a;
            
            return View(query.ToList()[0]);
            //return View();
        }

        public ActionResult SubmitRequest(String username, String company, int duration)
        {
            if (String.IsNullOrEmpty(username) || String.IsNullOrEmpty(company) || duration <= 0)
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            CurrentUserInfo info = getCurrentUserInfo();
            if (!ValidRequest(username, company, duration))
                return RedirectToAction("DuplicateRequest", new { id = DuplicateRequestID(username, company, duration) });
                //return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            if (!info.isActive) return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
            Request newRequest = new Request();
            //newRequest.UserID = getUserID();
            newRequest.Username = User.Identity.Name;
            //newRequest.AuthorizerID = 0;
            newRequest.Authorizer = "";
            newRequest.RequestName = username;
            newRequest.RequestCompany = company;
            newRequest.UserEMail = getEMailforUser();
            newRequest.TimeRequested = DateTime.Now;
            newRequest.Duration = duration;
            newRequest.EMailedAuthFlag = false;
            newRequest.EMailedRequesterFlag = false;

            if (info.isAuth)
            {
                newRequest.Status = (int)REQUEST_STATUS.PENDING;
            }
            else
            {
                if (!info.needsAuth && duration <= info.maxRequest)
                    newRequest.Status = (int)REQUEST_STATUS.PENDING;
                else
                    newRequest.Status = (int)REQUEST_STATUS.AUTH_REQ;
            }

            /*if (info.isAuth || !info.needsAuth || duration <= info.maxRequest)
                newRequest.Status = (int)REQUEST_STATUS.PENDING;
            else
                newRequest.Status = (int)REQUEST_STATUS.AUTH_REQ;*/
            newRequest.GenUserName = "";
            newRequest.GenPassword = "";
            //newRequest.AuthorizerID = BlankAuthID();
            ViewBag.Approved = (newRequest.Status == (int)REQUEST_STATUS.PENDING);
            if (ModelState.IsValid)
            {
                db.Requests.Add(newRequest);
                db.SaveChanges();
            }
            return View(newRequest);
        }

        public ActionResult UserGroups()
        {
            /*List<String> list = new List<String>();
            list.Add("Hello");
            list.Add("Bye");
            return View(list);*/
            //return View(AD.Groups("CN=Stuart Purnell - Head Office,OU=SP,OU=OPS Managers,DC=za,DC=illovo,DC=net", false));
            return View(AD.Groups(User.Identity.Name, false));
        }

        public ActionResult AlreadyProcessed()
        {
            return View();
        }

        public ActionResult Approve(int id)
        {
            CurrentUserInfo info = getCurrentUserInfo();
            if (!info.isAuth)
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
            var query =
                from a in db.Requests
                where (a.ID == id && a.Status == (int)REQUEST_STATUS.AUTH_REQ)
                select a;
            if (query.Count() > 0)
            {
                foreach (Request r in query)
                {
                    r.Status = (int)REQUEST_STATUS.AUTH_APPROVED;
                    r.TimeApproved = DateTime.Now;
                    r.Authorizer = User.Identity.Name;
                    break;
                }
                if (ModelState.IsValid)
                    db.SaveChanges();
            }
            else
            {
                return RedirectToAction("AlreadyProcessed");
            }

            query =
                from a in db.Requests
                where (a.ID == id)
                select a;

            return View(query.ToList()[0]);
        }

        public ActionResult Decline(int id)
        {
            CurrentUserInfo info = getCurrentUserInfo();
            if (!info.isAuth)
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
            var query =
                from a in db.Requests
                where (a.ID == id && a.Status == (int)REQUEST_STATUS.AUTH_REQ)
                select a;
            if (query.Count() > 0)
            {
                foreach (Request r in query)
                {
                    r.Status = (int)REQUEST_STATUS.AUTH_DECLINED;
                    r.TimeApproved = DateTime.Now;
                    r.Authorizer = User.Identity.Name;
                    break;
                }
                if (ModelState.IsValid)
                    db.SaveChanges();
            }
            else
            {
                return RedirectToAction("AlreadyProcessed");
            }

            query =
                from a in db.Requests
                where (a.ID == id)
                select a;

            return View(query.ToList()[0]);
        }

        //// GET: Requests
        //public ActionResult Index()
        //{
        //    var requests = db.Requests.Include(r => r.User).Include(r => r.Authorizer);
        //    return View(requests.ToList());
        //}

        //// GET: Requests/Details/5
        //public ActionResult Details(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Request request = db.Requests.Find(id);
        //    if (request == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(request);
        //}

        //// GET: Requests/Create
        //public ActionResult Create()
        //{
        //    ViewBag.UserID = new SelectList(db.Users, "ID", "UserName");
        //    ViewBag.AuthorizerID = new SelectList(db.Users, "ID", "UserName");
        //    return View();
        //}

        //// POST: Requests/Create
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Create([Bind(Include = "ID,UserID,UserEMail,Duration,Status,GenUserName,GenPassword,RequestName,RequestCompany,AuthorizerID")] Request request)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Requests.Add(request);
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }

        //    ViewBag.UserID = new SelectList(db.Users, "ID", "UserName", request.UserID);
        //    ViewBag.AuthorizerID = new SelectList(db.Users, "ID", "UserName", request.AuthorizerID);
        //    return View(request);
        //}

        //// GET: Requests/Edit/5
        //public ActionResult Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Request request = db.Requests.Find(id);
        //    if (request == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    ViewBag.UserID = new SelectList(db.Users, "ID", "UserName", request.UserID);
        //    ViewBag.AuthorizerID = new SelectList(db.Users, "ID", "UserName", request.AuthorizerID);
        //    return View(request);
        //}

        //// POST: Requests/Edit/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit([Bind(Include = "ID,UserID,UserEMail,Duration,Status,GenUserName,GenPassword,RequestName,RequestCompany,AuthorizerID")] Request request)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(request).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    ViewBag.UserID = new SelectList(db.Users, "ID", "UserName", request.UserID);
        //    ViewBag.AuthorizerID = new SelectList(db.Users, "ID", "UserName", request.AuthorizerID);
        //    return View(request);
        //}

        //// GET: Requests/Delete/5
        //public ActionResult Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Request request = db.Requests.Find(id);
        //    if (request == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(request);
        //}

        //// POST: Requests/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    Request request = db.Requests.Find(id);
        //    db.Requests.Remove(request);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
