﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Net.Mail;

namespace UserAdminBatchProcessor
{
    class Program
    {
        static UserAdminDBDataContext context = new UserAdminDBDataContext();
        
        static Config config;
        
        private enum REQUEST_STATUS { NO_STATUS, PENDING, AUTH_REQ, COMPLETE, AUTH_DECLINED, AUTH_APPROVED, CLEANED };
        //static string webServer = "http://localhost:19095/";
        //static string webServer = "http://hosrv.za.illovo.net/WiFiUserAuth/";
        static string webServer;
        //static string addGroupSID = "S-1-5-21-1960408961-1123561945-725345543-38776";
        static string addGroupSID;

        static void EndProg()
        {
            Console.WriteLine("Program complete.");
            Console.WriteLine("Strike ENTER to terminate.");
            Console.ReadLine();
        }

        static void ListRequests()
        {
            UserAdminDBDataContext db = new UserAdminDBDataContext();
            var query =
                from a in db.Requests
                select a;

            foreach (Request p in query)
                Console.WriteLine(p.RequestName);
        }

        static void PrintRequestList(List<Request> list, String caption = "")
        {
            if (!String.IsNullOrEmpty(caption))
                Console.WriteLine(caption);
            foreach (Request r in list)
                Console.WriteLine(" " + r.RequestName);
        }

        static List<Request> GetNewAutoApproveRequests(UserAdminDBDataContext db)
        {
            var query =
                from a in db.Requests
                where (a.Status == (int)REQUEST_STATUS.PENDING)
                select a;
            return query.ToList();
        }

        static List<Request> GetNewManualApproveRequests(UserAdminDBDataContext db)
        {
            var query =
                from a in db.Requests
                where (a.Status == (int)REQUEST_STATUS.AUTH_REQ && !a.EMailedAuthFlag)
                select a;
            return query.ToList();
        }

        static List<Request> GetManualApproveRequests(UserAdminDBDataContext db)
        {
            var query =
                from a in db.Requests
                where (a.Status == (int)REQUEST_STATUS.AUTH_APPROVED)
                select a;
            return query.ToList();
        }

        static List<Request> GetManualDeclineRequests(UserAdminDBDataContext db)
        {
            var query =
                from a in db.Requests
                where (a.Status == (int)REQUEST_STATUS.AUTH_DECLINED)
                select a;
            return query.ToList();
        }

        static String GenerateUserName()
        {
            Random r = new Random();
            int count = 6;
            String username = "";
            for (int i = 0; i < count; i++)
            {
                char c = (char)r.Next(65, 90);
                username += c;
            }
            return username;
        }

        static String GenerateUserName(String name)
        {
            Random r = new Random();
            int count = 3;
            String username = "";
            while (true)
            {
                username = "";
                for (int i = 0; i < count; i++)
                {
                    char c = (char)r.Next(65, 90);
                    username += c;
                }
                username = MakeUserName(name) + "." + username;
                if (!userExists(username)) break;
            }
            return username.ToLower();
        }

        static string MakeUserName(String name)
        {
            String username = "";
            for (int i = 0; i < name.Length; i++)
            {
                if (name[i] == ' ')
                    username += ".";
                else
                    username += name[i];
            }
            return username;
        }

        static bool userExists(String username)
        {
            PrincipalContext ouContext = new PrincipalContext(ContextType.Domain, config.Domain, config.AccountOU);
            UserPrincipal user = UserPrincipal.FindByIdentity(ouContext, username);
            return (user != null);
        }

        static String JumbleString(String val)
        {
            return val;
        }

        static int CountLowerCase(String line)
        {
            int count = 0;
            for (int i = 0; i < line.Length; i++)
                if (line[i] >= 'a' && line[i] <= 'z')
                    count++;
            return count;
        }

        static int CountUpperCase(String line)
        {
            int count = 0;
            for (int i = 0; i < line.Length; i++)
                if (line[i] >= 'A' && line[i] <= 'Z')
                    count++;
            return count;
        }

        static int CountNumbers(String line)
        {
            int count = 0;
            for (int i = 0; i < line.Length; i++)
                if (line[i] >= '0' && line[i] <= '9')
                    count++;
            return count;
        }

        static Boolean PasswordValid(String password)
        {
            if (password.Length >= 7 && CountLowerCase(password) > 1 && CountUpperCase(password) > 1 && CountNumbers(password) > 1)
                return true;
            return false;
        }

        static String NewPassword()
        {
            Random r = new Random();
            int charCount = 4;
            int numCount = 3;
            String password = "";
            for (int i = 0; i < charCount; i++)
            {
                char c = (char)r.Next(65, 91);
                if (r.Next(0, 2) == 1)
                    password += c.ToString().ToLower();
                else
                    password += c.ToString();
            }
            for (int i = 0; i < numCount; i++)
            {
                int n = r.Next(0, 9);
                password += n.ToString();
            }

            return JumbleString(password);
        }

        static String GeneratePassword()
        {
            String password = "";
            while (true)
            {
                if (PasswordValid(password))  break;
                password = NewPassword();
            }
            return password;
        }

        static Boolean CreateUser(String username, String password, int expiration, String person, String company, String requester)
        {
            Boolean success = true;
            String description = "WiFi account for " + person + " (" + company + "), requested by " + requester;
            
            PrincipalContext ouContex = new PrincipalContext(ContextType.Domain, config.Domain, config.AccountOU);

            try
            {
                UserPrincipal up = new UserPrincipal(ouContex);
                up.SamAccountName = username;
                //up.UserPrincipalName = username + "@za.illovo.net";
                up.UserPrincipalName = username + "@" + config.Domain;
                up.SetPassword(password);
                up.Description = description;
                up.AccountExpirationDate = DateTime.Now.AddHours(expiration);
                up.Enabled = true;
                GroupPrincipal addGroup = GroupPrincipal.FindByIdentity(ouContex, addGroupSID);
                
                //up.ExpirePasswordNow();
                up.Save();
                addGroup.Members.Add(up);
                addGroup.Save();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                success = false;
            }
            return success;
        }

        static void EMailPerson(String to, String subject, String body, Boolean html = false)
        {
            //SmtpClient client = new SmtpClient("exchhub.za.illovo.net");
            SmtpClient client = new SmtpClient(config.SmtpServerHost);
            //MailMessage message = new MailMessage("noreply@illovo.co.za", to, subject, body);
            MailMessage message = new MailMessage(config.SmtpFromAddress, to, subject, body);
            message.IsBodyHtml = html;
            try
            {
                if (!String.IsNullOrEmpty(to))
                    client.Send(message);
            }

            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        static String EMailSubject()
        {
            return "WiFi Access Request (" + config.FriendlyName + ")";
        }

        static void ProcessNewAutoApprovals(UserAdminDBDataContext db, List<Request> approvals)
        {
            foreach (Request r in approvals)
            {
                var query =
                    from a in db.Requests
                    where (a.ID == r.ID)
                    select a;
                foreach (Request u in query)
                {
                    //u.GenUserName = GenerateUserName();
                    u.GenUserName = GenerateUserName(u.RequestName);
                    u.GenPassword = GeneratePassword();
                    CreateUser(u.GenUserName, u.GenPassword, u.Duration, u.RequestName, u.RequestCompany, u.Username);
                    u.Status = (int)REQUEST_STATUS.COMPLETE;
                    u.TimeCompleted = DateTime.Now;
                    //String subject = "WiFi Access Request";
                    String subject = EMailSubject();
                    String body = "Dear " + u.Username + "\n";
                    body = body + "Your request for WiFi access has been approved for " + u.RequestName + " (" + u.RequestCompany + ").\n\n";
                    body = body + "The logon details are as follows:\n";
                    body = body + "Username:\t" + u.GenUserName + "\n";
                    body = body + "Password:\t" + u.GenPassword + "\n\n";
                    body = body + "This access will expire in " + u.Duration.ToString() + " hours.\n";
                    body = body + "\n\n" + "The SSID of this network is " + config.SSID;
                    EMailPerson(u.UserEMail, subject, body);
                    break;
                }
                db.SubmitChanges();
            }
        }

        static List<UserPrincipal> GetUsersFromGroup(GroupPrincipal group)
        {
            List<UserPrincipal> list = new List<UserPrincipal>();
            foreach (var u in group.Members)
            {
                list.Add((UserPrincipal)u);
            }
            return list;
        }

        static string GetNetbiosDomainName(string dnsDomainName)
        {
            string netbiosDomainName = string.Empty;

            DirectoryEntry rootDSE = new DirectoryEntry(string.Format("LDAP://{0}/RootDSE", dnsDomainName));

            string configurationNamingContext = rootDSE.Properties["configurationNamingContext"][0].ToString();

            DirectoryEntry searchRoot = new DirectoryEntry("LDAP://cn=Partitions," + configurationNamingContext);

            DirectorySearcher searcher = new DirectorySearcher(searchRoot);
            searcher.SearchScope = SearchScope.OneLevel;
            searcher.PropertiesToLoad.Add("netbiosname");
            searcher.Filter = string.Format("(&(objectcategory=Crossref)(dnsRoot={0})(netBIOSName=*))", dnsDomainName);

            SearchResult result = searcher.FindOne();

            if (result != null)
            {
                netbiosDomainName = result.Properties["netbiosname"][0].ToString();
            }

            return netbiosDomainName;
        }

        static List<UserPrincipal> approvers(UserAdminDBDataContext db)
        {
            List<UserPrincipal> users = new List<UserPrincipal>();
            var pquery =
                from a in db.Permissions
                where (a.IsAuthorizer)
                select a;
            foreach (Permission p in pquery)
            {
                foreach (Group g in p.Groups)
                {
                    //Console.WriteLine(g.SID);
                    //PrincipalContext ouContex = new PrincipalContext(ContextType.Domain, "za.illovo.net", "OU=WiFiAccess,OU=POC Test Users,DC=za,DC=illovo,DC=net");
                    PrincipalContext ouContex = new PrincipalContext(ContextType.Domain, config.Domain);
                    GroupPrincipal groupPrincipal = GroupPrincipal.FindByIdentity(ouContex, g.SID);
                    //Console.WriteLine(groupPrincipal.Name);
                    List<UserPrincipal> userList = GetUsersFromGroup(groupPrincipal);
                    foreach (UserPrincipal u in userList)
                    {
                        //String nb = GetNetbiosDomainName(u.UserPrincipalName.Split('@')[1]);
                        //users.Add(nb + "\\" + u.SamAccountName);
                        users.Add(u);
                    }
                        
                    
                }
            }
            return users;
        }

        static void ProcessNewManualApprovals(UserAdminDBDataContext db, List<Request> approvals)
        {
            List<UserPrincipal> app = approvers(db);

            foreach (Request r in approvals)
            {
                var query =
                    from a in db.Requests
                    where (a.ID == r.ID)
                    select a;
                foreach (Request rn in query)
                {
                    /*String subject = "WiFi Access Request";
                    String body = "Dear " + "" + "\n";
                    body = body + "Your request for WiFi access has been declined by " + reject.Authorizer + ".\n\n";
                    body = body + "Please contact them for more information.\n";
                    EMailPerson(reject.UserEMail, subject, body);*/
                    //List<String> app = approvers(db);
                    foreach (UserPrincipal user in app)
                    {
                        //String subject = "WiFi Access Request";
                        String subject = EMailSubject();
                        String approveURL = webServer + "Requests/Approve?id=" + rn.ID.ToString();
                        String declineURL = webServer + "Requests/Decline?id=" + rn.ID.ToString();
                        String body = "Dear " + user.Name + "<br /><br />";
                        body = body + "A new request has been submitted for WiFi access.<br />";
                        body = body + "The details are as follows:<br /><br />";
                        body = body + "Requestor:\t" + rn.Username + "<br />";
                        body = body + "User:\t" + rn.RequestName + "<br />";
                        body = body + "Company:\t" + rn.RequestCompany + "<br />";
                        body = body + "Duration:\t" + rn.Duration + "<br /><br />";
                        body = body + "Please choose one of the options below:<br />";
                        body = body + "<a href=\"" + approveURL + "\">Approve</a> | <a href=\"" + declineURL + "\">Decline</a><br />";
                        EMailPerson(user.EmailAddress, subject, body, true);
                        rn.EMailedAuthFlag = true;
                    }
                    break;
                }
                db.SubmitChanges();
            }
        }

        static void ProcessManualApprovals(UserAdminDBDataContext db, List<Request> approvals)
        {
            foreach (Request r in approvals)
            {
                var query =
                    from a in db.Requests
                    where (a.ID == r.ID)
                    select a;
                foreach (Request u in query)
                {
                    //u.GenUserName = GenerateUserName();
                    u.GenUserName = GenerateUserName(u.RequestName);
                    u.GenPassword = GeneratePassword();
                    CreateUser(u.GenUserName, u.GenPassword, u.Duration, u.RequestName, u.RequestCompany, u.Username);
                    u.Status = (int)REQUEST_STATUS.COMPLETE;
                    u.TimeCompleted = DateTime.Now;
                    //String subject = "WiFi Access Request";
                    String subject = EMailSubject();
                    String body = "Dear " + u.Username + "\n";
                    body = body + "Your request for WiFi access has been approved for " + u.RequestName + " (" + u.RequestCompany + ").\n\n";
                    body = body + "The logon details are as follows:\n";
                    body = body + "Username:\t" + u.GenUserName + "\n";
                    body = body + "Password:\t" + u.GenPassword + "\n\n";
                    body = body + "This access will expire in " + u.Duration.ToString() + " hours.\n";
                    body = body + "\n\n" + "The SSID of this network is " + config.SSID;
                    EMailPerson(u.UserEMail, subject, body);
                    break;
                }
                db.SubmitChanges();
            }
        }

        static void ProcessNewManualDeclines(UserAdminDBDataContext db, List<Request> approvals)
        {
            foreach (Request r in approvals)
            {
                var query =
                    from a in db.Requests
                    where (a.ID == r.ID)
                    select a;
                foreach (Request reject in query)
                {
                    reject.Status = (int)REQUEST_STATUS.COMPLETE;
                    reject.TimeCompleted = DateTime.Now;
                    //String subject = "WiFi Access Request";
                    String subject = EMailSubject();
                    String body = "Dear " + reject.Username + "\n";
                    body = body + "Your request for WiFi access  for " + reject.RequestName + " (" + reject.RequestCompany + ") has been declined by " + reject.Authorizer + ".\n\n";
                    body = body + "Please contact them for more information.\n";
                    EMailPerson(reject.UserEMail, subject, body);
                    break;
                }
                db.SubmitChanges();
            }
        }

        static void setConfig()
        {
            var q =
                from a in context.Configs
                select a;
            if (q.Count() > 0)
            {
                foreach (var elem in q)
                {
                    config = elem;
                    break;
                }
            }
            else
            {
                throw (new Exception("Config table contains no rows."));
            }
            webServer = config.WebServerURL;
            addGroupSID = config.AddGroupSID;
        }

        static void CleanupAccounts(UserAdminDBDataContext db)
        {
            var q =
                from a in db.Requests
                where (a.Status == (int)REQUEST_STATUS.COMPLETE)
                select a;
            PrincipalContext ouContext = new PrincipalContext(ContextType.Domain, config.Domain, config.AccountOU);
            foreach (var elem in q)
            {
                if (elem.TimeRequested < DateTime.Now.AddHours(-elem.Duration))
                {
                    Console.WriteLine("Removing user " + elem.GenUserName + ".");
                    UserPrincipal user = UserPrincipal.FindByIdentity(ouContext, elem.GenUserName);
                    if (user != null)
                    {
                        bool success = true;
                        try
                        {
                            user.Delete();

                        }
                        catch (Exception e)
                        {
                            success = false;
                            Console.WriteLine(e.Message);
                        }
                        if (success)
                            elem.Status = (int)REQUEST_STATUS.CLEANED;
                    }
                }
            }
            db.SubmitChanges();
        }

        static void ProcessRequests()
        {
            setConfig();
            UserAdminDBDataContext db = new UserAdminDBDataContext();
            List<Request> autoApprove = GetNewAutoApproveRequests(db);      // Auto approve these ones.
            List<Request> newManualApprove = GetNewManualApproveRequests(db); // New requests that need to be set to manual approve.
            List<Request> manualApprove = GetManualApproveRequests(db);    // Manual approved requests that have been approved.
            List<Request> manualDecline = GetManualDeclineRequests(db);    // Manual approved requests that have been declined.
            ProcessNewAutoApprovals(db, autoApprove);
            ProcessNewManualApprovals(db, newManualApprove);
            ProcessManualApprovals(db, manualApprove);
            ProcessNewManualDeclines(db, manualDecline);
            PrintRequestList(autoApprove, "Auto Approve");
            PrintRequestList(newManualApprove, "New Manual Approve");
            PrintRequestList(manualApprove, "Manual Approve to Process");
            PrintRequestList(manualDecline, "Manual Decline to Process");
            CleanupAccounts(db);
        }

        static void Main(string[] args)
        {
            //ListRequests();
            ProcessRequests();
            //EndProg();
        }
    }
}
